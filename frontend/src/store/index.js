import Vue from 'vue';
import Vuex from 'vuex';
import createDataTree from './utls';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    comments: [{ id: 1, text: 'Первый' }, { id: 2, text: 'Второй', parentid: 1 }, { id: 3, text: 'Первый' }, { id: 4, text: 'Второй' }],
  },
  getters: {
    treedComments: (state) => createDataTree(state.comments),
  },
  actions: {
    addReply({ state }, reply) {
      state.comments.push(reply);
    },
  },
  modules: {
  },
});
