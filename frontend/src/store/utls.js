// Source https://stackoverflow.com/a/40732240/7415288
function createDataTree(dataset) {
  const hashTable = Object.create(null);
  dataset.forEach((aData) => {
    hashTable[aData.id] = { ...aData, children: [] };
  });
  const dataTree = [];
  dataset.forEach((aData) => {
    if (aData.parentid) hashTable[aData.parentid].children.push(hashTable[aData.id]);
    else dataTree.push(hashTable[aData.id]);
  });
  console.log(dataTree);
  return dataTree;
}

export default createDataTree;
